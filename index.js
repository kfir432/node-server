const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
//morgan is for logging
const morgan = require('morgan');
const app = express();
const router = require('./router');

const mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost:auth/auth').then(function(e){
//    console.log(e);
// });
mongoose.connect('mongodb://localhost/auth', function(err, db) {
    if (err) {
        console.log('Unable to connect to the server. Please start the server. Error:', err);
    } else {
        console.log('Connected to Server successfully!');
    }
});
//App Setup

app.use(morgan('combined'));
app.use(bodyParser.json({type:'*/*'}));
router(app);

//Server Setup

const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);

console.log('Server listening on:',port);